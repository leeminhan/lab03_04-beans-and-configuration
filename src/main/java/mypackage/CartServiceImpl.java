package mypackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CartServiceImpl implements CartService {

    // Autowire the catalog bean you just created.
    // This bean will be the source of catalog data for your service bean.
    @Value("#{catalog}")
    private Map<Integer, Item> catalog; // id: item

    @Autowired
    private CartRepository repository;

    @Value("${contactEmail}")
    private String contactEmail;

    @Value("${onlineRetailer.salesTaxRate}")
    private double salesTaxRate;

    @Value("${onlineRetailer.deliveryCharge.normal}")
    private double standardDeliveryCharge;

    @Value("${onlineRetailer.deliveryCharge.threshold}")
    private double deliveryChargeThreshold;

    public String getContactEmail() {
        return contactEmail;
    }

    @Override
    public void addItemToCart(int id, int quantity) {
        if (catalog.get(id) != null) {
            repository.add(id, quantity);
        }
    }

    @Override
    public void removeItemFromCart(int id) {
        repository.remove(id);
    }

    @Override
    public Map<Integer, Integer> getAllItemsInCart() {
        return repository.getAll();
    }

    @Override
    public double calculateCartCost() {
        Map<Integer, Integer> items = repository.getAll();

        double totalCost = 0;

        for (Map.Entry<Integer, Integer> item: items.entrySet()){
            int id = item.getKey();
            int quantity = item.getValue();

            double itemCost = catalog.get(id).getPrice() * quantity;
            totalCost += itemCost;
        }
        return totalCost;
    }

    public double calculateSalesTax() {
        return salesTaxRate * calculateCartCost();
    }

    public double calculateDeliveryCharge() {
        double totalCost = calculateCartCost();
        if (totalCost == 0 || totalCost >= deliveryChargeThreshold) {
            return 0;
        } else {
            return standardDeliveryCharge;
        }
    }
}
