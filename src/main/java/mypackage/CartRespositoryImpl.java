package mypackage;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CartRespositoryImpl implements CartRepository {

    private HashMap<Integer, Integer> cart = new HashMap<>();

    @Override
    public void add(int itemId, int quantity) {
        Integer exisitingQuantity = cart.get(itemId);
        if (exisitingQuantity != null) {
            cart.put(itemId, exisitingQuantity + 1);
        }
        cart.put(itemId, quantity);
    }

    @Override
    public void remove(int itemId) {
        cart.remove(itemId);
    }

    @Override
    public Map<Integer, Integer> getAll() {
        return cart;
    }
}
